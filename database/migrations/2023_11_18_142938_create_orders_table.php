<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignUuid('user_id')->constrained('users');
            // $table->foreignId('payment_method_id')->constrained('payment_methods');
            $table->dateTime('pickup_date');
            $table->dateTime('payment_date')->nullable();
            $table->dateTime('delivery_date');
            $table->string('transaction_code')->nullable();
            $table->string('address');
            $table->string('payment_status');
            $table->string('snap_token')->nullable();
            $table->integer('price');
            $table->enum('status', ['pickup', 'picked_up', 'counting', 'washing', 'waiting_payment', 'deliver', 'completed', 'cancelled'])->default('pickup');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
