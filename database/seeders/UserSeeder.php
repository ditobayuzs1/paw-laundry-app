<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        DB::table('users')->insert([
            [
                'id' => '1135c042-775a-4294-8225-2367150d3a94',
                'name' => 'Dito Bayu',
                'email' => 'asd@gmail.com',
                'password' => Hash::make('asdasdasd'),
                'role' =>'user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69e',
                'name' => 'John Doe',
                'email' => 'qwe@gmail.com',
                'password' => Hash::make('qweqweqwe'),
                'role' =>'user',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'name' => 'Zxc zxc',
                'email' => 'zxc@gmail.com',
                'password' => Hash::make('zxczxczxc'),
                'role' =>'admin',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69i',
                'name' => 'Admin Aguskenyang',
                'email' => 'guskeny@gmail.com',
                'password' => Hash::make('kenyangbuk'),
                'role' =>'admin',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}
