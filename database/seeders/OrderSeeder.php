<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Order::insert([
            [
                'user_id' => '1135c042-775a-4294-8225-2367150d3a94',
                // 'payment_method_id' => 1,
                'pickup_date' => now(),
                'delivery_date' => now()->addDays(3),
                'transaction_code' => null,
                'address' => '15 Hiclory Lane, Silver Spring, DC, Washington, 20940',
                'payment_status' => 'pending',
                'snap_token' => null,
                'price' => 100000,
                'status' => 'pickup',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69e',
                // 'payment_method_id' => 2,
                'pickup_date' => now(),
                'delivery_date' => now()->addDays(3),
                'transaction_code' => null,
                'address' => '15 Hiclory Lane, Silver Spring, DC, Washington, 20940',
                'payment_status' => 'pending',
                'snap_token' => null,
                'price' => 100000,
                'status' => 'pickup',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'user_id' => '1135c042-775a-4294-8225-2367150d3a94',
                // 'payment_method_id' => 3,
                'pickup_date' => now(),
                'delivery_date' => now()->addDays(3),
                'transaction_code' => null,
                'address' => '15 Hiclory Lane, Silver Spring, DC, Washington, 20940',
                'payment_status' => 'pending',
                'snap_token' => null,
                'price' => 100000,
                'status' => 'pickup',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
