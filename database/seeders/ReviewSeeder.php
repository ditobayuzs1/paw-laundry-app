<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('reviews')->insert([
            [
                'user_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'review' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.',
                'rating' => 5,
            ],
            [
                'user_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69e',
                'review' => 'awokawokoakwawk',
                'rating' => 4,
            ],
        ]);
    }
}
