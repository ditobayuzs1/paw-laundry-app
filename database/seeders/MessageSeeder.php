<?php

namespace Database\Seeders;

use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Message::insert([
            
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'haloo Admin10',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'haloo Dito20',
                'created_at' => Carbon::now()->addSeconds(20)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'apa kabar?0',
                'created_at' => Carbon::now()
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'baik kak',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'haloo Admin',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'haloo Dito',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'apa kabar?',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'baik kak',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'haloo Admin',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'haloo Dito',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'apa kabar?',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'baik kak',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'haloo Admin',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'haloo Dito',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'apa kabar?',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'baik kak',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'haloo Admin',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'haloo Dito',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '1135c042-775a-4294-8225-2367150d3a94',
                'message' => 'apa kabar?',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
            [
                'chat_room_id' => 1,
                'sender_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'message' => 'baik kak',
                'created_at' => Carbon::now()->addSeconds(10)
            ],
        ]);
    }
}
