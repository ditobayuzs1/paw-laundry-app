<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ChatRoom;
class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ChatRoom::insert([
            [
                'admin_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'user_id' => '1135c042-775a-4294-8225-2367150d3a94',
            ],
            [
                'admin_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69a',
                'user_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69e',
            ],
            [
                'admin_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69i',
                'user_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69e',
            ],
            [
                'admin_id' => '320f0b4b-ecb5-4010-9a7e-9e73161ec69i',
                'user_id' => '1135c042-775a-4294-8225-2367150d3a94',
            ],
        ]);
    }
}
