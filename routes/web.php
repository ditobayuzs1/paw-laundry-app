<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReviewController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

// Dito

Route::post('midtrans/notification', [PaymentController::class, 'midtransCallback']);

// Route::get('/welcome', function () {
//     return inertia('Welcome');
// })->name('order');
Route::middleware(['auth'])->group(function () {
    Route::prefix('review')->group(function () {
        Route::get('/', [ReviewController::class, 'index']);
    });
});

Route::middleware(['auth'])->group(function () {

    Route::prefix('profile')->group(function () {
        Route::get('/', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/', [ProfileController::class, 'destroy'])->name('profile.destroy');
    });


    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::middleware(['userAccess:user'])->group(function () {

        // Dito
        Route::prefix('order')->group(function () {
            Route::get('/', [OrderController::class, 'index'])->name('order');
            Route::get('create', [OrderController::class, 'createOrder'])->name('create-order');
            Route::post('create-order', [OrderController::class, 'order'])->name('post-create-order');
            Route::get('status/{order_id}', [OrderController::class, 'orderStatus'])->name('order-status');
            Route::get('history', [OrderController::class, 'orderHistory'])->name('order-history');
            Route::get('details/{order_id}', [OrderController::class, 'orderDetails'])->name('order-details');
            Route::post('pay', [PaymentController::class, 'store'])->name('pay');
        });
        // Nazwa
        Route::prefix('customer-service')->group(function () {
            Route::get('/', [ChatController::class, 'userChat'])->name('user-chat');
            Route::get('/{admin_id}', [ChatController::class, 'userChatRoom']);
            Route::post('/', [ChatController::class, 'userSendMessage'])->name('user-send-message');
        });

        //yasir
        Route::prefix('review')->group(function () {
            Route::get('/', [ReviewController::class, 'index'])->name('review.index');
            Route::get('/edit{id}', [ReviewController::class, 'edit'])->name('review.edit');
            Route::put('/update/{id}', [ReviewController::class, 'update'])->name('review.update');
            Route::delete('/review/{id}', [ReviewController::class, 'destroy'])->name('review.destroy');
            Route::get('/create', [ReviewController::class, 'create'])->name('review.create');
            Route::post('/review', [ReviewController::class, 'store'])->name('review.store');
        });
    });

    Route::middleware(['userAccess:admin'])->group(function () {
        // ferdinand
        Route::prefix('admin')->group(function () {
            Route::get('/', [AdminController::class, 'admin']);
            //Nazwa
            Route::get('chat', [ChatController::class, 'adminChat'])->name('admin-chat');
            Route::get('chat/{customer_id}', [ChatController::class, 'adminChatRoom']);
            Route::post('chat', [ChatController::class, 'adminSendMessage'])->name('admin-send-message');

            //Dito
            Route::get('/order-list', [OrderController::class, 'orderList'])->name('order-list');
            Route::put('/order-list', [OrderController::class, 'orderEditStatus'])->name('order-edit-status');

            //yasir
            Route::get('review', [ReviewController::class, 'adminReview'])->name('admin-review');
            Route::delete('/review/{id}', [ReviewController::class, 'adminDestroy'])->name('review.adestroy');
        });
    });
});

//
Route::get('/reviews', [ReviewController::class, 'index']);
require __DIR__ . '/auth.php';
