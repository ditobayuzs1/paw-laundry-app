import { Link, Head } from "@inertiajs/react";

export default function Welcome({ auth, laravelVersion, phpVersion }) {
    return (
        <div className="bg-gray-100 dark:bg-gray-900 text-gray-800 dark:text-gray-100">
            <Head title="Welcome" />
            <div className="w-full fixed z-40 bg-white flex flex-row items-center py-4 justify-between px-20 text-md dark:bg-gray-800">
                <img src="/images/logo.svg" alt="logo" className="h-16 w-16" />

                <div className="hidden space-x-8 sm:-my-px sm:ms-10 sm:flex">
                    <div className="flex flex-row items-center gap-8">
                        <a href="#home">Home</a>
                        <a href="#about">About Us</a>
                        <a href="#service">Services</a>
                        {/* <a href="#gallery">Gallery</a> */}
                        {auth.user != null ? (
                            <Link href={route("dashboard")} className="">
                                Dashboard
                            </Link>
                        ) : null}
                    </div>
                </div>
                {auth.user == null ? (
                    <div className="flex flex-row items-center gap-2">
                        <Link
                            href={route("login")}
                            className="py-2 px-4 hover:scale-90 duration-200 rounded-xl dark:bg-gray-900 bg-gray-100"
                        >
                            Login
                        </Link>
                        <Link
                            href={route("register")}
                            className="py-2 px-4 hover:scale-90 duration-200 rounded-xl dark:bg-gray-900 bg-gray-100"
                        >
                            Register
                        </Link>
                    </div>
                ) : (
                    <div></div>
                )}
            </div>
            <div className="max-w-7xl mx-auto">
                <div className="flex flex-col"></div>
            </div>
            {/* test */}
            <div className="flex-col justify-center mx-auto flex">
                {/* home */}
                <div
                    id="home"
                    className="max-w-7xl mx-auto px-28 pt-48 pb-12 flex-col justify-center items-center gap-28 flex"
                >
                    <div className="flex-col justify-center items-center gap-12 flex">
                        <div className="self-stretch text-center text-6xl font-semibold">
                            Laundry App
                        </div>
                        <div className="self-stretch text-center text-xl">
                            Hadirkan kenyamanan laundry ke dalam genggaman Anda
                            dengan Laundry App. Kami menghadirkan pengalaman
                            laundry yang lebih cepat, efisien, dan praktis. Tak
                            perlu lagi khawatir tentang jadwal atau antrean
                            panjang. Layanan laundry yang sesuai dengan gaya
                            hidup modern Anda.
                        </div>
                    </div>
                    <a
                        href="#about"
                        className="py-3 px-6 hover:scale-90 duration-200 rounded-xl dark:bg-gray-800 bg-white "
                    >
                        Read More
                    </a>
                </div>
                {/* about */}
                <div id="about" className=" dark:bg-gray-800 bg-white w-full">
                    <div className=" max-w-7xl mx-auto py-20 flex-col justify-start items-center gap-24 flex">
                        <div className="self-stretch flex-col justify-center items-center gap-12 flex">
                            <div className="w-96 text-center text-4xl font-semibold">
                                About Us
                            </div>
                            <div className="flex-col justify-center px-12 items-center gap-7 flex">
                                <div className="text-center text-xl">
                                    Our services are not only fast, clean,
                                    efficient, they are also very reliable. Our
                                    customer care executives are especially
                                    trained to provide guidance and ensure
                                    on-time delivery.
                                </div>
                            </div>
                        </div>
                        <div className="self-stretch justify-center items-center gap-24 flex">
                            <div className="flex-col justify-start items-end gap-16 flex">
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-right text-xl font-semibold">
                                            WASH
                                        </div>
                                        <div className="w-80 text-right text-md font-normal">
                                            Kotoran dan noda akan menjadi
                                            kenangan setelah layanan cuci kami.
                                            Pakaian bersih dan segar tanpa
                                            repot!
                                            <br />
                                        </div>
                                    </div>
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/wash.svg" alt="" />
                                    </div>
                                </div>
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-right text-xl font-semibold">
                                            DRY
                                        </div>
                                        <div className="w-80 text-right text-md font-normal">
                                            Tidak suka menunggu? Fitur kering
                                            kami membuat pakaian Anda kering
                                            seketika. Cepat, efisien, dan siap
                                            dipakai!
                                            <br />
                                        </div>
                                    </div>
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/dry.svg" alt="" />
                                    </div>
                                </div>
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-right text-xl font-semibold">
                                            IRON
                                        </div>
                                        <div className="w-80 text-right text-md font-normal">
                                            Tampilan rapi tanpa usaha! Kami
                                            menjamin setiap lipatan rapi dan
                                            pakaian yang terlihat seperti baru
                                            kembali.
                                            <br />
                                        </div>
                                    </div>
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/iron.svg" alt="" />
                                    </div>
                                </div>
                            </div>
                            <div className="w-80 h-80 justify-center items-center flex">
                                <div className="w-80 h-80 relative">
                                    <img src="/images/logo.svg" alt="logo" />
                                </div>
                            </div>
                            <div className="flex-col justify-start items-end gap-16 flex">
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/bedding.svg" alt="" />
                                    </div>
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-xl font-semibold">
                                            BEDDING
                                        </div>
                                        <div className="w-80 text-md font-normal">
                                            Peralatan tidur yang bersih dan
                                            nyaman adalah kunci mimpi nyenyak.
                                            Biarkan kami merawat seprai, sarung
                                            bantal, dan selimut Anda.
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/speedy.svg" alt="" />
                                    </div>
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-xl font-semibold">
                                            SPEEDY
                                        </div>
                                        <div className="w-80 text-md font-normal">
                                            Butuh pakaian cepat? Layanan kilat
                                            kami menjamin hasil terbaik dalam
                                            waktu singkat. Tanpa mengorbankan
                                            kualitas!
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div className="justify-start items-start gap-10 flex">
                                    <div className="w-12 h-12 relative">
                                        <img src="/images/stream.svg" alt="" />
                                    </div>
                                    <div className="flex-col justify-start items-end gap-3.5 flex">
                                        <div className="self-stretch text-xl font-semibold">
                                            STREAM
                                        </div>
                                        <div className="w-80 text-md font-normal">
                                            Pakaian segar sepanjang hari! Fitur
                                            uap kami memberikan sentuhan akhir
                                            yang membuat pakaian terasa lebih
                                            segar dan bebas kusam.
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* service */}
                <div
                    id="service"
                    className="max-w-7xl mx-auto pt-20 pb-12 flex-col items-center gap-12 flex"
                >
                    <h2 className="text-4xl font-semibold">Service</h2>
                    <div className="flex flex-row mx-20">
                        <img
                            src="/images/delivery.svg"
                            alt=""
                            className="w-1/3"
                        />
                        <div className="flex flex-col justify-between py-8 px-12">
                            <div className="flex flex-col gap-4">
                                <h3 className="text-3xl font-semibold">
                                    Delivered To Your Doorstep
                                </h3>
                                <p>
                                    Kami mengambil pakaian kotor Anda dan
                                    mengembalikannya dalam waktu 24 jam. Kami
                                    menjamin keamanan pakaian Anda dengan sistem
                                    pelacakan yang canggih.
                                </p>
                            </div>
                            <Link
                                href="/order/create"
                                className="py-2 px-4 w-fit rounded-full hover:scale-95 duration-200 bg-white dark:bg-gray-800"
                            >
                                Order Now
                            </Link>
                        </div>
                    </div>
                </div>
                {/* <div className="py-36 flex-col justify-start items-start gap-20 flex">
                        <div className="w-96 px-28 justify-start items-start gap-2.5 flex">
                            <div className="grow shrink basis-0 text-center text-6xl font-semibold">
                                Gallery
                            </div>
                        </div>
                        <div className="justify-end items-center flex">
                            <div className="self-stretch shadow justify-start items-start gap-12 flex">
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="pl-14 justify-end items-center flex">
                            <div className="self-stretch shadow justify-start items-start gap-12 flex">
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-72 h-72 relative">
                                    <div className="w-72 h-72 left-0 top-0 absolute bg-white rounded-2xl" />
                                    <div className="w-64 h-64 left-[15px] top-[15px] absolute rounded-3xl">
                                        <div className="w-64 h-64 left-0 top-0 absolute bg-white bg-opacity-90 rounded-2xl border-8 border-zinc-700" />
                                        <div className="w-80 h-px left-[5px] top-[12px] absolute origin-top-left rotate-[44.51deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="w-80 h-px left-[237px] top-[4px] absolute origin-top-left rotate-[134.75deg] opacity-90 border-8 border-zinc-700"></div>
                                        <div className="p-1 left-[86px] top-[105.50px] absolute bg-white bg-opacity-90 justify-start items-start gap-2.5 flex">
                                            <div className="opacity-90 text-center text-zinc-700 text-2xl font-normal">
                                                Image
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */}
                <div className="dark:bg-gray-800 bg-white">
                    <div className=" max-w-7xl mx-auto py-12 flex-col justify-start items-center gap-12 flex">
                        <div className="flex flex-row justify-between items-start">
                            <img
                                src="/images/logo.svg"
                                alt=""
                                className="w-1/6"
                            />
                            <div className="flex flex-col text-center justify-center">
                                <h3 className="text-3xl mb-4 font-semibold">
                                    Our Location
                                </h3>
                                <span>Gedung F</span>
                                <span>
                                    Fakultas Ilmu Komputer Universitas Brawijaya
                                </span>
                                <span>
                                    Jalan Veteran Nomor 8, Lowokwaru, Malang,
                                    Jawa Timur 65145
                                </span>
                            </div>
                            <div className="flex flex-col text-right justify-center">
                                <h3 className="text-3xl mb-4 font-semibold">
                                    Contact Us
                                </h3>
                                <span>Phone: 08123456789</span>
                                <span>Email : qwe@gmail.com</span>
                            </div>
                        </div>
                        <div className="self-stretch text-center text-xl font-semibold">
                            ©2023, PAW Kelompok 4
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
