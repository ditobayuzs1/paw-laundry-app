import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import React, { useEffect, useRef } from "react";

export default function Chat({ auth, chat_room_id, chats, admin }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        admin_id: admin.id,
        chat_room_id: chat_room_id,
        message: "",
    });
    console.log(chats);

    const submit = (e) => {
        e.preventDefault();

        post(route("user-send-message"), {
            onSuccess: () => reset("message"),
        });
    };

    const bottomRef = useRef();

    useEffect(() => {
        bottomRef.current.scrollIntoView({ behavior: "smooth" });
    });
    return (
        // <AuthenticatedLayout user={auth.user}>

        <div className="bg-gray-100 dark:bg-gray-900 h-screen">
            <Head title="Chat" />
            <div className="">
                <div className="relative overflow-hidden shadow-sm text-gray-900 dark:text-gray-100">
                    <div className="items-center flex flex-row fixed py-6 px-12 gap-8 bg-gray-100 dark:bg-gray-900 w-full">
                        <Link
                            href={route("user-chat")}
                            className="hover:scale-90 duration-200"
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="currentColor"
                                class="bi bi-chevron-left h-8 w-8"
                                viewBox="0 0 16 16"
                            >
                                <path
                                    fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
                                />
                            </svg>
                        </Link>
                        <h2 className="text-2xl font-semibold">{admin.name}</h2>
                    </div>
                    <div className="flex flex-col bg-gray-100 dark:bg-gray-900  gap-2 px-28 py-28">
                        {chats.length != 0 ? (
                            chats.map((val) => (
                                <div
                                    className={`justify-end flex ${
                                        val.sender_id == auth.user.id
                                            ? "flex-row"
                                            : "flex-row-reverse"
                                    } gap-4`}
                                >
                                    <div className="rounded-lg bg-white dark:bg-gray-800 p-4 w-fit">
                                        <p>{val.message}</p>
                                    </div>
                                    <img
                                        src="/images/clothes.png"
                                        className="rounded-full h-12 w-12"
                                    />
                                </div>
                            ))
                        ) : (
                            <div className="flex justify-center">
                                <h2 className="text-4xl mb-8 opacity-60">
                                    Tidak ada pesan
                                </h2>
                            </div>
                        )}
                    </div>
                    <div ref={bottomRef} className="opacity-0">
                        .
                    </div>
                    <form
                        onSubmit={submit}
                        className="fixed bottom-0 w-full px-28 py-4 bg-gray-100 dark:bg-gray-900"
                    >
                        <div className="bottom-0 flex flex-row gap-4">
                            <input
                                placeholder="Let me know if you have any more questions!"
                                type="text"
                                className="w-full rounded-full px-4 self-stretch bg-white dark:bg-gray-800 border-none focus:ring-0"
                                onChange={(event) =>
                                    setData("message", event.target.value)
                                }
                                value={data.message}
                            />
                            <button className="rounded-full h-12 w-12 p-4 hover:scale-95 duration-200 bg-white dark:bg-gray-800">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="currentColor"
                                    class="bi bi-send h-full w-full"
                                    viewBox="0 0 16 16"
                                >
                                    <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z" />
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        // </AuthenticatedLayout>
    );
}
