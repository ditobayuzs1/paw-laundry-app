import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import React, { useEffect, useRef } from "react";

export default function Chat({ auth, chat_room_id, chats, customer }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        customer_id: customer.id,
        chat_room_id: chat_room_id,
        message: "",
    });
    console.log(chats);

    const submit = (e) => {
        e.preventDefault();

        post(route("admin-send-message"), {
            onSuccess: () => reset("message"),
        });
    };

    const bottomRef = useRef();

    useEffect(() => {
        bottomRef.current.scrollIntoView({ behavior: "smooth" });
    });
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Chat" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="relative overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            <h2 className="text-2xl font-semibold">
                                {customer.name}
                            </h2>
                        </div>
                        <div className="flex flex-col gap-2">
                            {chats.length != 0 ? (
                                chats.map((val) => (
                                    <div
                                        className={`justify-end flex ${
                                            val.sender_id == auth.user.id
                                                ? "flex-row"
                                                : "flex-row-reverse"
                                        } gap-4`}
                                    >
                                        <div className="rounded-lg bg-white dark:bg-gray-800 p-4 w-fit">
                                            <p>{val.message}</p>
                                        </div>
                                        <img
                                            src="/images/clothes.png"
                                            className="rounded-full h-12 w-12"
                                        />
                                    </div>
                                ))
                            ) : (
                                <div className="flex justify-center">
                                    <h2 className="text-4xl mb-8 opacity-60">
                                        Tidak ada pesan
                                    </h2>
                                </div>
                            )}
                        </div>
                        <form onSubmit={submit} ref={bottomRef}>
                            <div className="bottom-0 flex flex-row gap-4 mt-4">
                                <input
                                    type="text"
                                    className="w-full rounded-full px-4 self-stretch bg-white dark:bg-gray-800 border-none focus:ring-0"
                                    onChange={(event) =>
                                        setData("message", event.target.value)
                                    }
                                    value={data.message}
                                />
                                <button className="rounded-full h-12 w-12 p-4 hover:scale-95 duration-200 bg-white dark:bg-gray-800">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="currentColor"
                                        class="bi bi-send h-full w-full"
                                        viewBox="0 0 16 16"
                                    >
                                        <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576zm6.787-8.201L1.591 6.602l4.339 2.76 7.494-7.493Z" />
                                    </svg>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
