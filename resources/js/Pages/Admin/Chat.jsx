import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link } from "@inertiajs/react";

export default function Chat({ auth, chat_rooms }) {
    console.log(chat_rooms);
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Chat" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            <h2 className="text-2xl font-semibold">Customer</h2>
                        </div>
                        <div className="flex gap-4 flex-col">
                            {chat_rooms
                                ? chat_rooms.map((val) => (
                                      <Link
                                          href={`chat/${val.user.id}`}
                                          className="rounded-2xl hover:scale-95 duration-200 bg-white dark:bg-gray-800 py-4 px-8 flex flex-row justify-between"
                                      >
                                          <div className="flex flex-col">
                                              <h2 className="text-2xl font-semibold">
                                                  {val.user.name}
                                              </h2>
                                              <span className="opacity-60">
                                                  {val?.latestMessage?.message
                                                      ? val.latestMessage
                                                            .message
                                                      : "Tidak ada pesan"}
                                              </span>
                                          </div>
                                          <span className="opacity-60">
                                              {val?.latestMessage?.sent_at
                                                  ? val.latestMessage.sent_at
                                                  : null}
                                          </span>
                                      </Link>
                                  ))
                                : null}
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
