import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import React, { useEffect, useState } from "react";

export default function CreateOrder({ auth, orderList }) {
    const { data, setData, put, processing, errors, reset } = useForm({
        order_id: null,
        price: null,
    });

    const submit = (e) => {
        put(route("order-edit-status"));
    };
    console.log(orderList);
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Order" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            <h2 className="text-xl font-semibold">
                                Order List
                            </h2>
                        </div>

                        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
                            <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                    <tr>
                                        <th scope="col" className="px-6 py-3">
                                            Customer
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            <div className="flex items-center">
                                                Email
                                            </div>
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            <div className="flex items-center">
                                                Price
                                            </div>
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            <div className="flex items-center">
                                                Status
                                            </div>
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            <span className="sr-only">
                                                Edit
                                            </span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {orderList
                                        ? orderList.map((val) => (
                                              <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                                  <th
                                                      scope="row"
                                                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                                                  >
                                                      {val.user.name}
                                                  </th>
                                                  <td className="px-6 py-4">
                                                      {val.user.email}
                                                  </td>
                                                  <td className="">
                                                      <input
                                                          readOnly={
                                                              val.status ==
                                                              "counting"
                                                                  ? false
                                                                  : true
                                                          }
                                                          type="number"
                                                          className={
                                                              (val.price === 0
                                                                  ? "text-red-400"
                                                                  : "") +
                                                              (val.status ==
                                                              "counting"
                                                                  ? " bg-gray-200 rounded-xl dark:bg-gray-700"
                                                                  : " cursor-default bg-transparent ") +
                                                              " px-6 py-4 border-none focus:ring-0 text-gray-900 dark:text-white"
                                                          }
                                                          onChange={(event) => {
                                                              console.log(
                                                                  event.target
                                                                      .value
                                                              );
                                                              setData(
                                                                  "price",
                                                                  event.target
                                                                      .value
                                                              );
                                                          }}
                                                          value={
                                                              val.price === 0
                                                                  ? val.status ==
                                                                    "counting"
                                                                      ? data.price
                                                                      : val.price
                                                                  : val.price
                                                              //   data.price
                                                              //       ? data.price
                                                              //       : val.price ===
                                                              //         0
                                                              //       ? "Isi harga"
                                                              //       : val.price
                                                          }
                                                      />
                                                  </td>
                                                  <td className="px-6 py-4">
                                                      {val.status}
                                                  </td>
                                                  <td className="px-6 py-4 text-right">
                                                      <form onSubmit={submit}>
                                                          <button
                                                              onClick={() => {
                                                                  setData(
                                                                      "order_id",
                                                                      val.id
                                                                  );
                                                              }}
                                                              value={val.id}
                                                              href="#"
                                                              className="font-medium text-blue-600 dark:text-blue-500 py-2 px-4 rounded-xl hover:scale-90 duration-200 bg-gray-100 dark:bg-gray-900"
                                                          >
                                                              Next Status
                                                          </button>
                                                      </form>
                                                  </td>
                                              </tr>
                                          ))
                                        : "Tidak ada Order"}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
