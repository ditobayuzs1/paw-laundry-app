import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link } from "@inertiajs/react";

export default function Index({ auth }) {
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Order" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            <h2 className="text-2xl font-semibold">
                                Hey, {auth.user.name}!
                            </h2>
                            <h2 className="text-2xl font-semibold">
                                What type of clothes will you laundry today?
                            </h2>
                        </div>
                        <div className="grid grid-cols-4 gap-4">
                            <div className="flex flex-col gap-2 p-4 justify-center bg-white dark:bg-gray-800">
                                <img
                                    src="/images/clothes.png"
                                    className="object-cover h-44 w-full self-center"
                                />
                                <h2 className="text-xl font-semibold">
                                    Casual
                                </h2>
                                <h2 className="text-md font-semibold">
                                    laundry laundry laundry laundry laundry
                                </h2>
                                <h2 className="text-md opacity-50">
                                    Rp6.000/kg
                                </h2>
                            </div>
                            <div className="flex flex-col gap-2 p-4 justify-center bg-white dark:bg-gray-800">
                                <img
                                    src="/images/sheets.jpg"
                                    className="object-cover h-44 w-full self-center"
                                />
                                <h2 className="text-xl font-semibold">Sheet</h2>
                                <h2 className="text-md font-semibold">
                                    laundry laundry laundry laundry laundry
                                </h2>
                                <h2 className="text-md opacity-50">
                                    Rp10.000/kg
                                </h2>
                            </div>
                            <div className="flex flex-col gap-2 p-4 justify-center bg-white dark:bg-gray-800">
                                <img
                                    src="/images/towel.jpg"
                                    className="object-cover h-44 w-full self-center"
                                />
                                <h2 className="text-xl font-semibold">Towel</h2>
                                <h2 className="text-md font-semibold">
                                    laundry laundry laundry laundry laundry
                                </h2>
                                <h2 className="text-md opacity-50">
                                    Rp8.000/kg
                                </h2>
                            </div>
                            <div className="flex flex-col gap-2 p-4 justify-center bg-white dark:bg-gray-800">
                                <img
                                    src="/images/blanket.jpg"
                                    className="object-cover h-44 w-full self-center"
                                />
                                <h2 className="text-xl font-semibold">
                                    Blanked
                                </h2>
                                <h2 className="text-md font-semibold">
                                    laundry laundry laundry laundry laundry
                                </h2>
                                <h2 className="text-md opacity-50">
                                    Rp9.000/kg
                                </h2>
                            </div>
                        </div>
                        <div className="flex justify-center mt-8">
                            <Link
                                href="/order/create"
                                className="py-2 px-4 rounded-full hover:scale-95 duration-200 bg-white dark:bg-gray-800"
                            >
                                Order Now
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
