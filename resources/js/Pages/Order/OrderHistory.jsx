import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";

export default function CreateOrder({ auth, orderHistory }) {
    console.log(orderHistory);
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Order" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            <h2 className="text-xl font-semibold">
                                Order History
                            </h2>
                        </div>
                        {orderHistory
                            ? orderHistory.map((val) => (
                                  <div className="rounded-xl gap-4 flex flex-col bg-white mb-4 p-4 dark:bg-gray-800 self-stretch">
                                      <div className="flex flex-row items-center justify-between">
                                          <span className="opacity-60">
                                              {val.created_at}
                                          </span>
                                          <span className="font-semibold">
                                              Completed
                                          </span>
                                      </div>
                                      <hr />
                                      <div className="flex flex-row gap-8 self-stretch">
                                          <img
                                              src="/images/clothes.png"
                                              className="w-2/12"
                                          />
                                          <div className="flex gap-2 flex-col flex-1">
                                              <span className="font-semibold">
                                                  Casual
                                              </span>
                                              {/* <span className="opacity-60">
                                                  4kg
                                              </span> */}
                                              <div className="self-stretch flex flex-row justify-end">
                                                  <span className="opacity-60 ">
                                                      Rp.{val.price}
                                                  </span>
                                              </div>
                                          </div>
                                      </div>
                                      <hr />
                                      <div className="self-strect gap-2 flex flex-row justify-end">
                                          <span className=" ">
                                              Order total :
                                          </span>
                                          <span className=" ">
                                              Rp.{val.price}
                                          </span>
                                      </div>
                                      <div className="self-strect gap-2 flex flex-row justify-end">
                                          <a
                                              href={`/order/details/${val.id}`}
                                              className="py-2 px-4 rounded-full hover:scale-95 duration-200 bg-gray-100 dark:bg-gray-700"
                                          >
                                              See Details
                                          </a>
                                      </div>
                                  </div>
                              ))
                            : "Tidak ada history"}
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
