import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head } from "@inertiajs/react";

export default function CreateOrder({ auth, order }) {
    console.log(order);
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Order" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="bg-white mb-2 rounded-xl p-4 dark:bg-gray-800">
                            <h2 className="text-xl mb-4 font-semibold">
                                Order Completed
                            </h2>
                            <p>
                                Your order has been efficiently processed and
                                meticulously handled. Your garments are now
                                cleaned, pressed, and ready for you to enjoy,
                                marking the successful completion of your
                                laundry experience with us.
                            </p>
                        </div>
                        <div className="bg-white mb-2 rounded-xl p-4 dark:bg-gray-800">
                            <h2 className="text-xl mb-4 font-semibold">
                                Shipping Address
                            </h2>
                            <p>{order.user.name}</p>
                            <p>{order.address}</p>
                        </div>
                        <div className="bg-white mb-2 rounded-xl p-4 dark:bg-gray-800">
                            <h2 className="text-xl mb-4 font-semibold">
                                Payment Method
                            </h2>
                            <p>Bank Transfer - Virtual Account</p>
                        </div>
                        {/* items */}
                        <div className="rounded-xl gap-4 flex flex-col bg-white mb-4 p-4 dark:bg-gray-800 self-stretch">
                            <div className="flex flex-row items-center justify-between">
                                <span className="opacity-60">
                                    27 November 2023
                                </span>
                                <span className="font-semibold">Completed</span>
                            </div>
                            <hr />
                            <div className="flex flex-row gap-8 self-stretch">
                                <img
                                    src="/images/clothes.png"
                                    className="w-2/12"
                                />
                                <div className="flex gap-2 flex-col flex-1">
                                    <span className="font-semibold">
                                        Casual
                                    </span>
                                    {/* <span className="opacity-60">4kg</span> */}
                                    <div className="self-stretch flex flex-row justify-end">
                                        <span className="opacity-60 ">
                                            {order.price}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="self-strect gap-2 flex flex-row justify-end">
                                <span className=" ">Order total :</span>
                                <span className=" ">{order.price}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
