import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, useForm } from "@inertiajs/react";
import React, { useState } from "react";

export default function CreateOrder({ auth }) {
    const [isAddressSet, setIsAddressSet] = useState(true);

    const date = new Date();

    const { data, setData, post, processing, errors, reset } = useForm({
        address: "15 Hiclory Lane, Silver Spring, DC, Washington, 20940",
        pickup_date:
            date.getFullYear() +
            "-" +
            (date.getMonth() + 1) +
            "-" +
            date.getDate() +
            " " +
            date.getHours() +
            ":" +
            date.getMinutes() +
            ":" +
            date.getSeconds(),
        delivery_days: 0,
    });

    function getMonthName(monthNumber) {
        const date = new Date();
        date.setMonth(monthNumber - 1);

        return date.toLocaleString("en-US", {
            month: "long",
        });
    }

    const submit = (e) => {
        e.preventDefault();

        post(route("post-create-order"));
    };
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Order" />

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <form onSubmit={submit}>
                            <div className="mb-8">
                                <h2 className="text-xl font-semibold">
                                    Address
                                </h2>
                                <div className="self-stretch bg-white dark:bg-gray-800 p-4 rounded-xl mt-4 flex flex-row items-center justify-between">
                                    <input
                                        className={
                                            (isAddressSet
                                                ? "cursor-default"
                                                : "") +
                                            "text-xl font-semibold bg-transparent flex flex-1 border-none focus:ring-0"
                                        }
                                        readOnly={isAddressSet ? true : false}
                                        onChange={(event) =>
                                            setData(
                                                "address",
                                                event.target.value
                                            )
                                        }
                                        value={data.address}
                                    />

                                    <button
                                        type="button"
                                        onClick={() =>
                                            setIsAddressSet((prev) => !prev)
                                        }
                                        className="py-2 px-4 rounded-full hover:scale-95 duration-200 bg-gray-100 dark:bg-gray-700"
                                    >
                                        Change
                                    </button>
                                </div>
                            </div>
                            <div className="grid grid-cols-4 gap-4 p-4 rounded-2xl bg-white dark:bg-gray-800">
                                <div className="flex flex-col gap-2 p-4 justify-center rounded-2xl">
                                    <img
                                        src="/images/clothes.png"
                                        className="self-stretch"
                                    />
                                </div>
                                <div className="col-span-3 flex flex-col gap-2 p-4 rounded-2xl">
                                    <h2 className="text-xl font-semibold">
                                        Pick Up
                                    </h2>
                                    <div className="self-stretch text-md bg-white dark:bg-gray-800 rounded-xl flex flex-row items-center justify-between">
                                        <h2 className="">
                                            {getMonthName(date.getMonth() + 1)}{" "}
                                            {date.getDate()},{" "}
                                            {date.getFullYear()}
                                        </h2>
                                        <h2 className="">
                                            {date.getHours()}.00{" - "}
                                            {date.getHours() + 1}.00
                                        </h2>
                                    </div>
                                    <h2 className="text-xl font-semibold">
                                        Delivered by
                                    </h2>
                                    <div className="self-stretch text-md bg-white dark:bg-gray-800 rounded-xl flex flex-row items-center justify-between">
                                        <div className="flex flex-row items-center gap-4">
                                            <input
                                                type="radio"
                                                id="basic"
                                                name="delivered-by"
                                                value="basic"
                                                defaultChecked
                                                onChange={(event) =>
                                                    setData("delivery_days", 2)
                                                }
                                            />
                                            <label for="basic">
                                                2-3 work days
                                            </label>
                                        </div>
                                        <div className="flex flex-row items-center gap-4">
                                            <input
                                                type="radio"
                                                id="pro"
                                                name="delivered-by"
                                                value="pro"
                                                onChange={(event) =>
                                                    setData("delivery_days", 0)
                                                }
                                            />
                                            <label for="pro">
                                                tomorrow (+ Rp5000/kg)
                                            </label>
                                        </div>
                                    </div>
                                    <div className="flex flex-row justify-end mt-4">
                                        <button className="py-2 px-4 rounded-full hover:scale-95 duration-200 bg-gray-100 dark:bg-gray-700">
                                            Place Order
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
