import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import React, { useState } from "react";
import { Head } from "@inertiajs/react";
import { router } from "@inertiajs/react";

const OrderStatus = ({ order, env, snaptok, auth }) => {
    const [status, setStatus] = useState(order.status);
    const pay = (id) => {
        router.post(
            route("pay", {
                order_id: id,
            }),
            {},
            {
                only: ["order"],
                onSuccess: ({ props }) => {
                    onSnapMidtrans(props.order);
                },
            }
        );
    };

    const onSnapMidtrans = (order) => {
        snap.pay(order.snap_token, {
            onSuccess: function (result) {
                router.visit("/order/status/" + order.id);
            },
            onPending: function (result) {
                console.log(result);
            },
            onError: function (result) {
                console.log(result);
            },
        });
    };

    return (
        <AuthenticatedLayout user={auth.user}>
            <Head>
                <link
                    rel="stylesh-eet"
                    href="https://unpkg.com/flickity@2/dist/flickity.min.css"
                />
                <title>SubscriptionPlan</title>
                <script
                    src="https://app.sandbox.midtrans.com/snap/snap.js"
                    data-clientkey={env.MIDTRANS_CLIENTKEY}
                ></script>
                <title>Status</title>
            </Head>

            <div className="py-4">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className=" overflow-hidden shadow-sm p-6 text-gray-900 dark:text-gray-100">
                        <div className="mb-8">
                            {/* icon */}
                            <div className="self-stretch py-8 flex-col justify-start items-center gap-16 flex">
                                <div className="justify-start items-center flex">
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "pickup" ||
                                        status == "picked_up" ||
                                        status == "counting" ||
                                        status == "waiting_payment" ||
                                        status == "washing" ||
                                        status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "picked_up" ||
                                        status == "counting" ||
                                        status == "waiting_payment" ||
                                        status == "washing" ||
                                        status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "counting" ||
                                        status == "waiting_payment" ||
                                        status == "washing" ||
                                        status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "waiting_payment" ||
                                        status == "washing" ||
                                        status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "washing" ||
                                        status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "deliver" ||
                                        status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                    <div className="w-14 h-px border-4 border-zinc-700"></div>
                                    <div className="w-16 h-16 rounded-full border-4 border-zinc-700">
                                        {status == "completed" ? (
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="currentColor"
                                                className=" w-full h-full bi bi-check"
                                                viewBox="0 0 16 16"
                                            >
                                                <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                                            </svg>
                                        ) : null}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {status == "pickup" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/pickup.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Pickup
                                </h2>
                                <p className="text-xl text-center">
                                    Tim laundry lagi dalam perjalanan menuju
                                    tempatmu untuk mengambil cucian. Mereka siap
                                    memberikan layanan terbaik untuk membuat
                                    cucianmu bersih dan wangi.
                                </p>
                            </div>
                        ) : null}
                        {status == "picked_up" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/picked_up.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Picked Up
                                </h2>
                                <p className="text-xl text-center">
                                    Cucianmu sudah berhasil diambil oleh tim
                                    laundry. Sekarang, kamu bisa santai karena
                                    cucianmu berada dalam tangan yang ahli.
                                </p>
                            </div>
                        ) : null}
                        {status == "counting" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/counting.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Counting
                                </h2>
                                <p className="text-xl text-center">
                                    Sedang menghitung berat cucian untuk
                                    menentukan biaya layanan. Pastikan kamu siap
                                    membayar dengan dompet yang siap digunakan.
                                </p>
                            </div>
                        ) : null}
                        {status == "waiting_payment" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <h2 className="font-semibold text-4xl">
                                    Waiting for Payment
                                </h2>
                                <div className="rounded-xl gap-4 flex flex-col bg-white p-4 dark:bg-gray-800 self-stretch">
                                    <div className="flex flex-row items-center justify-between">
                                        <span className="opacity-60">
                                            27 November 2023
                                        </span>
                                        <span className="font-semibold">
                                            UNPAID
                                        </span>
                                    </div>
                                    <hr />
                                    <div className="flex flex-row gap-8 self-stretch">
                                        <img
                                            src="/images/clothes.png"
                                            className="w-2/12"
                                        />
                                        <div className="flex gap-2 flex-col flex-1">
                                            <span className="font-semibold">
                                                Casual
                                            </span>
                                            <span className="opacity-60"></span>
                                            <div className="self-stretch flex flex-row justify-end">
                                                <span className="opacity-60 ">
                                                    Rp{order.price}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="self-strect flex flex-row justify-end">
                                        <span className=" ">
                                            Order total : Rp
                                        </span>
                                        <span className=" ">{order.price}</span>
                                    </div>
                                    <div className="self-strect gap-2 flex flex-row justify-end">
                                        <button
                                            onClick={() => pay(order.id)}
                                            className="py-2 px-4 rounded-full hover:scale-95 duration-200 bg-gray-100 dark:bg-gray-700"
                                        >
                                            Pay Now
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ) : null}
                        {status == "washing" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/washing.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Washing
                                </h2>
                                <p className="text-xl text-center">
                                    Cucianmu sedang berada dalam mesin cuci dan
                                    siap untuk mendapatkan perawatan khusus.
                                    Bersiaplah untuk menyambut cucianmu yang
                                    akan lebih bersih dan harum.
                                </p>
                            </div>
                        ) : null}
                        {status == "deliver" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/deliver.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Deliver
                                </h2>
                                <p className="text-xl text-center">
                                    Cucianmu sudah bersih dan dikemas dengan
                                    rapi. Mereka sekarang dalam perjalanan
                                    menuju tempatmu, jadi pastikan untuk siap
                                    menerima kedatangan mereka.
                                </p>
                            </div>
                        ) : null}
                        {status == "completed" ? (
                            <div className="flex flex-col gap-8 p-4 items-center rounded-2xl">
                                <img
                                    src="/images/completed.svg"
                                    className="w-44"
                                />
                                <h2 className="font-semibold text-4xl">
                                    Completed
                                </h2>
                                <p className="text-xl text-center">
                                    Taraa! Pesananmu udah nyampe dengan selamat.
                                    Cek hasil laundrynya, semoga sesuai
                                    ekspektasi dan baju-bajumu jadi lebih
                                    kinclong!
                                </p>
                            </div>
                        ) : null}
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
};

export default OrderStatus;
