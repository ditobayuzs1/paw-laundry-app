<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="icon" href="/favicon.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="theme-color" content="#000000" />
  <title>Review - Laravel</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter%3A500"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A500%2C600"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins%3A600"/>
  <link rel="icon" type="image/x-icon" href="/images/logo.svg">
  <link rel="stylesheet" href="/css/style-review.css" />
</head>
<body>
<header>
  <div class="header-content">
    <div class="logo">
      <img src="/images/logo.svg" alt="Company Logo">
    </div>
    <div class="header-links">
      <a href="/order" class="customer-service-link">Customer Service</a>
      <a href="/order/history" class="order-link">Order</a>
      <a href="/customer-service" class="order-history-link">Order History</a>
      <a href="/review" class="review-link">Review</a>
    </div>
  </div>
</header>

<div class="page-review">
  <div class="auto-group-wcxd-XQZ">

    @if ($userReviews->isEmpty())
    <p class="intro-review">Silahkan Memberikan Review </p>
        <!-- <div class="button-uT3">Review</div> -->
        <a href="{{route('review.create')}}" class="button-uT3">Review</a>
    @else
    <p class="intro-review">Review Anda </p>
        <div class="button-uT3" style="display: none;">Review</div>
    @endif
  </div>
  @if ($userReviews->isEmpty())
  <div class="auto-group-uuad-CqT">
          <div class="temp-kosong">Anda belum melakukan review</div>
  </div>
  @else
  <div class="auto-group-uuad-CqT">
      <div class="auto-group-1qdb-Szh">
        @foreach($userReviews as $review)
          <div class="nama-Mbs">Nama</div>
          <div class="name-user">{{$review->user->name}}</div>
      </div>
      <div class="auto-group-ywt1-azR">
          <div class="rating-jMX">Rating</div>
          <div class="rating-data">{{$review->rating}}</div>
      </div>
          <div class="text-review">{{$review->review}}</div>
  </div>
  <div class="auto-group-wecd-yHo">
          <a href="{{ route('review.edit' ,['id' => $review->id]) }}" class="button-HJV">Update</a>
          <form class="button-AFo" action="{{ route('review.destroy', $review->id) }}" method="POST">
          @csrf
          @method('DELETE')
          <button type="submit" class="button-AFo" >Hapus</button>
          </form>

  </div>
        @endforeach
  @endif
</div>
</body>
