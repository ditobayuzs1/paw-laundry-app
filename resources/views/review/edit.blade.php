<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <title>Update Review - Laravel</title>
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins%3A600"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A600"
    />
  <link rel="icon" type="image/x-icon" href="/images/logo.svg">
    <link rel="stylesheet" href="/css/style-edit-review.css" />
  </head>
  <body>
        <div class="page-form-Umb">
            <div class="auto-group-2v7f-mVo">Update Review</div>
            <div class="auto-group-kgsr-piy">
                <form action ="{{route('review.update',['id' => $review->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                <div class="auto-group-6qhj-wYh">
                    <div class="rating-h25">Rating</div>
                    <input class="rating-input-1" type="number" name="rating" min="1" max="100" />
                </div>
                <div class="auto-group-aksp-fND">
                    <div class="review-ojK">Review</div>
                    <textarea class="review-input-1" name="review" rows="4" cols="50"></textarea>
                </div>
                    <button type="submit" class="button-S1b">Update Data</button>
            </div>
                </form>
        </div>
  </body>
</html>
