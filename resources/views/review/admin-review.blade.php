<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Review Table</title>
  <link rel="icon" type="image/x-icon" href="/images/logo.svg">
  <style>
    body {
  margin: 0;
  font-family: Arial, sans-serif;
}
.review-table {
  width: 100%;
  border-collapse: collapse;
  border: 1px solid #ccc;
}

.review-table th, .review-table td {
  border: 1px solid #ccc;
  padding: 8px;
  text-align: left;
}

.review-table th {
  background-color: #f2f2f2;
}

.delete-button {
  padding: 6px 12px;
  background-color: #ff6347;
  color: #fff;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

.delete-button:hover {
  background-color: #ff4837;
}


header {
    background-color: #ffffff;
    padding: 10px 0;
    text-align: center;
}

.header-content {
    max-width: 1200px;
    margin: 0 auto;
    display: flex;
    justify-content: flex-start;
    align-items: center;
}

.logo img {
    max-height: 50px;
}

.header-links a {
    padding: 8px 16px;
    color: black;
    text-decoration: none;
    border-radius: 4px;
    margin-right: 10px; 
}

.header-links a:first-child {
    margin-right: 15px; 
}

.header-links .review-link {
    border-bottom: solid 3px #00d1ff;
}
.header-links a:hover {
}

main {
  text-align: center;
  padding: 20px 0;
}

.table-container {
  max-width: 800px;
  margin: 0 auto;
}

.review-table {
  width: 100%;
  border-collapse: collapse;
  border: 1px solid #ccc;
}



  </style>
</head>
<body>

<header>
  <div class="header-content">
    <div class="logo">
      <img src="/images/logo.svg" alt="Company Logo">
    </div>
    <div class="header-links">
      <a href="/order" class="order-link">Order</a>
      <a href="/order/history" class="order-history-link">Order History</a>
      <a href="/admin/chat" class="customer-service-link">Customer Service</a>
      <a href="/review" class="review-link">Review</a>
    </div>
  </div>
</header>

<main>
  <div class="table-container">
    <table class="review-table">
    <thead>
    <tr>
      <th>Nomor</th>
      <th>nama</th>
      <th>Rating</th>
      <th>Review</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  @foreach($reviews as $key => $data)
  
 
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$data->user->name}}</td>
      <td>{{$data->rating}}</td>
      <td>{{$data->review}}</td>
      <td>
        <form class="delete-button" action="{{ route('review.adestroy', $data->id) }}" method="POST">
          @csrf
          @method('DELETE')
          <button type="submit" class="delete-button" >Hapus</button>
          </form>
      </td>
    </tr>
  @endforeach
  </tbody>
    </table>
  </div>
</main>

</body>
</html>
