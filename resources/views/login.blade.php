<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>
    <link rel="stylesheet" href="/css/style-login.css" />

</head>
<body>
    <div class="sign-in">
        <div class="div">
          <div class="content">
            <div class="title">
              <div class="text-wrapper">Hey, hello 👋</div>
              <p class="p">Welcome back, you've been missed!</p>
            </div>
            <div class="content-2">
              <div class="form">
                <div class="email-form">
                  <input type="text" placeholder="Email">
                </div>
                <div class="password-form">
                  <input type="password" placeholder="Password">
                  <img class="eye-slash" src="images/Eye Slash.png" />
                </div>
              </div>
              <div class="text-wrapper-3"><button>Forgot password?</button></div>
              {{-- <button class="sign-in-form">Sign In</button> --}}
              <button class="label"><div class="text-wrapper">Sign Up</div></button>
              <div class="or-sign-in">
                <img class="line" src="images/Line 10.png" />
                <div class="text-wrapper-5">or sign in with</div>
                <img class="line" src="images/Line 10.png" />
              </div>
              <div class="google">
                <div class="ellipse"></div>
                <img class="icons-google" src="images/google.png" />
              </div>
              <div class="sign-up">
                <p class="don-t-have-an">
                  <span class="span">Don’t have an account?</span>
                  <span class="text-wrapper-6">&nbsp;</span>
                  <button class="text-wrapper-7">Sign Up</button>
                </p>
              </div>
            </div>
          </div>
          <img class="illustration" src="images/illustration.png" />
        </div>
      </div>   
</body>
</html>