<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index()
    {

        $userId = auth()->user()->id;

        $userReviews = Review::where('user_id', $userId)->get();
        return view('review.reviews-index', ['userReviews' => $userReviews]);
    }
    public function create()
    {
        return view('review.create');
    }
    public function store(Request $request)
    {
        $userReviews = Review::create($request->all());
        return redirect()->route('review.index');
    }
    public function edit($id)
    {
        $review = Review::findorFail($id);
        return view('review.edit', ['review' => $review]);
    }
    public function update(Request $request, $id)
    {
        $review = Review::find($id);
        $review->update($request->all());
        return redirect()->route('review.index');
    }
    public function destroy($id)
    {
        $review = Review::find($id);
        $review->delete();
        return redirect()->route('review.index');
    }
    public function adminReview()
    {
        $reviews = Review::all();
        return view('review.admin-review',['reviews'=>$reviews]);
    }
    public function adminDestroy($id)
    {
        $review = Review::find($id);
        $review->delete();
        return redirect()->route('admin-review');
    }
}
