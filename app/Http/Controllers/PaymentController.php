<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Midtrans\Notification;
use Midtrans\Snap;
use Carbon\Carbon;

class PaymentController extends Controller
{


    function __construct()
    {
        \Midtrans\Config::$serverKey = env("MIDTRANS_SERVERKEY");
        \Midtrans\Config::$isProduction = env("MIDTRANS_IS_PRODUCTION");
        \Midtrans\Config::$isSanitized = env("MIDTRANS_IS_SANITIZED");
        \Midtrans\Config::$is3ds = env("MIDTRANS_IS_3DS");
    }

    public function store(Request $request)
    {

        $order = Order::find($request->order_id);

        $params = [
            'transaction_details' => [
                'order_id' => $order->id . "-" . Str::random(5),
                'gross_amount' => $order->price,
            ]
        ];
        $snapToken = Snap::getSnapToken($params);

        $order->update([
            'status' => 'washing',
            'transaction_code' => strtoupper(Str::random(10)),
            "snap_token" => $snapToken
        ]);

        return inertia('Order/OrderStatus', ['order' => $order]);
    }

    public function midtransCallback(Request $request)
    {
        $notif = new Notification();


        $transaction_status = $notif->transaction_status;
        $fraud = $notif->fraud_status;

        $order_id = explode('-', $notif->order_id)[0];
        $order = Order::find($order_id);

        if ($transaction_status == 'capture') {
            if ($fraud == 'challenge') {
                $order->payment_status = 'pending';
            } else if ($fraud == 'accept') {
                $order->payment_status = 'paid';
                $order->payment_date = Carbon::now();
            }
        } else if ($transaction_status == 'cancel') {
            if ($fraud == 'challenge') {
                $order->payment_status = 'failed';
            } else if ($fraud == 'accept') {
                $order->payment_status = 'failed';
            }
        } else if ($transaction_status == 'deny') {
            $order->payment_status = 'failed';
        } else if ($transaction_status == 'settlement') {
            $order->payment_status = 'paid';
            $order->payment_date = Carbon::now();
        } else if ($transaction_status == 'pending') {
            $order->payment_status = 'pending';
        } else if ($transaction_status == 'expire') {
            $order->payment_status = 'failed';
        }

        $order->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Payment success'
        ], 200);
    }
}
