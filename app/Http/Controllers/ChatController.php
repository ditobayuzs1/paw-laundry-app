<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChatRoom;
use App\Models\Message;
use App\Models\User;

class ChatController extends Controller
{
    public function adminChat()
    {
        $chatRooms = ChatRoom::where('admin_id', auth()->user()->id)->get();
        foreach ($chatRooms as $chatRoom) {
            $chatRoom->user;
            $latestMessage = Message::where('chat_room_id', $chatRoom->id)->latest()->first();
            if ($latestMessage) {
                $latestMessage->sent_at = $latestMessage->created_at->diffForHumans();
            } 
            $chatRoom->latestMessage = $latestMessage;
        }
        return inertia('Admin/Chat', ['chat_rooms' => $chatRooms]);
    }
    public function adminChatRoom(Request $request)
    {
        $chatRoom = ChatRoom::where('admin_id', auth()->user()->id)->where('user_id', $request->customer_id)->first();
        $customer = User::find($request->customer_id);
        $chats = Message::where('chat_room_id', $chatRoom->id)->oldest()->get();
        foreach ($chats as $chat) {
            $chat->sender;
        }
        return inertia('Admin/ChatRoom', ['chats' => $chats, 'chat_room_id' => $chatRoom->id, 'customer' =>  $customer]);
    }
    public function adminSendMessage(Request $request)
    {

        $message = Message::create([
            'chat_room_id' => $request->chat_room_id,
            'sender_id' => auth()->user()->id,
            'message' => $request->message,
        ]);

        return redirect('admin/chat/' . $request->customer_id);
    }
    public function userChat()
    {
        $chatRooms = ChatRoom::where('user_id', auth()->user()->id)->get();

        foreach ($chatRooms as $chatRoom) {
            $chatRoom->admin;
            $latestMessage = Message::where('chat_room_id', $chatRoom->id)->latest()->first();
            if ($latestMessage) {
                $latestMessage->sent_at = $latestMessage->created_at->diffForHumans();
            } 
            $chatRoom->latestMessage = $latestMessage;
        }
        return inertia('User/Chat', ['chat_rooms' => $chatRooms]);
    }
    public function userChatRoom(Request $request)
    {
        $chatRoom = ChatRoom::where('user_id', auth()->user()->id)->where('admin_id', $request->admin_id)->first();
        $admin = User::find($request->admin_id);
        $chats = Message::where('chat_room_id', $chatRoom->id)->oldest()->get();
        foreach ($chats as $chat) {
            $chat->sender;
        }
        return inertia('User/ChatRoom', ['chats' => $chats, 'chat_room_id' => $chatRoom->id, 'admin' =>  $admin]);
    }
    public function userSendMessage(Request $request)
    {

        $message = Message::create([
            'chat_room_id' => $request->chat_room_id,
            'sender_id' => auth()->user()->id,
            'message' => $request->message,
        ]);

        return redirect('customer-service/' . $request->admin_id);
    }
}
