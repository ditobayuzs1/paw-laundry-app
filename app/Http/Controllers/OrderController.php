<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;


class OrderController extends Controller
{
    public function index()
    {
        return inertia('Order/Index');
    }

    public function createOrder()
    {
        return inertia('Order/CreateOrder');
    }

    public function order(Request $request)
    {
        $order = Order::create([
            'user_id' => auth()->user()->id,
            'pickup_date' => $request->pickup_date,
            'address' => $request->address,
            'delivery_date' => now()->addDays($request->delivery_days),
            'transaction_code' => null,
            'payment_status' => 'pending',
            'snap_token' => null,
            'price' => 0,
            'status' => 'pickup',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        return redirect('order/status/' . $order->id);
    }

    public function orderHistory()
    {
        $orderHistory = Order::where('user_id', auth()->user()->id)->get();
        return inertia('Order/OrderHistory', ['orderHistory' => $orderHistory]);
    }

    public function orderList()
    {
        $orderList = Order::all();
        foreach ($orderList as $order) {
            $order->user;
        }
        return inertia('Order/OrderList', ['orderList' => $orderList]);
    }

    public function orderEditStatus(Request $request)
    {
        $order = Order::find($request->order_id);
        switch ($order->status) {
            case 'pickup':
                $order->update(['status' => 'picked_up']);
                break;
            case 'picked_up':
                $order->update(['status' => 'counting']);
                break;
            case 'counting':
                $order->update(['price' => $request->price]);
                $order->update(['status' => 'waiting_payment']);
                break;
            case 'waiting_payment':
                $order->update(['status' => 'washing']);
                break;
            case 'washing':
                $order->update(['status' => 'deliver']);
                break;
            case 'deliver':
                $order->update(['status' => 'completed']);
                break;
        }
        return redirect(route('order-list'));
    }
    public function orderEditPrice(Request $request)
    {
        $order = Order::find($request->order_id);
        $order->update(['price' => $request->price]);
        return redirect(route('order-list'));
    }

    public function orderDetails(Request $request)
    {
        $order = Order::find($request->order_id);
        if (!$order) {
            return redirect()->route('order-history');
        }
        $order->user;
        return inertia('Order/OrderDetails', ['order' => $order]);
    }

    public function orderStatus(Request $request)
    {
        $order = Order::find($request->order_id);
        if (!$order) {
            return redirect()->route('order');
        }
        return inertia('Order/OrderStatus', ['order' => $order]);
    }

    // public function show(Order $order)
    // {
    //     return response()->json(['order' => $order]);
    // }

    // public function store(Request $request)
    // {
    //     $validatedData = $request->validate([]);

    //     $order = Order::create($validatedData);

    //     return response()->json(['message' => 'Order created successfully', 'order' => $order]);
    // }

    // public function update(Request $request, Order $order)
    // {
    //     $validatedData = $request->validate([]);

    //     $order->update($validatedData);

    //     return response()->json(['message' => 'Order updated successfully', 'order' => $order]);
    // }

    // public function destroy(Order $order)
    // {
    //     $order->delete();

    //     return response()->json(['message' => 'Order deleted successfully']);
    // }

    // public function pickedUp(Order $order)
    // {
    //     $order->update(['status' => 'picked_up', 'picked_date' => now()]);

    //     return response()->json(['message' => 'Order status set to picked_up']);
    // }

    // public function counting(Order $order)
    // {
    //     $order->update(['status' => 'counting']);

    //     return response()->json(['message' => 'Order status set to counting']);
    // }

    // public function waitingPayment(Order $order)
    // {
    //     $order->update(['status' => 'waiting_payment']);

    //     return response()->json(['message' => 'Order status set to waiting_payment']);
    // }

    // public function washing(Order $order)
    // {
    //     $order->update(['status' => 'washing']);

    //     return response()->json(['message' => 'Order status set to washing']);
    // }

    // public function deliver(Order $order)
    // {
    //     $order->update(['status' => 'deliver']);

    //     return response()->json(['message' => 'Order status set to washing']);
    // }

    // public function completed(Order $order)
    // {
    //     $order->update(['status' => 'completed']);

    //     return response()->json(['message' => 'Order status set to completed']);
    // }

    // public function cancelled(Order $order)
    // {
    //     $order->update(['status' => 'cancelled']);

    //     return response()->json(['message' => 'Order status set to cancelled']);
    // }
}
