<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected  $fillable = [
        'user_id',
        'payment_method_id',
        'pickup_date',
        'delivery_date',
        'payment_date',
        'status',
        'transaction_code',
        "snap_token",
        'payment_status',
        'price',
        'address',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
